<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Customers;
use App\Models\Employees;
use App\Models\OrderDetails;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Shippers;
use App\Models\Suppliers;
use App\Models\Territories;

use Illuminate\Http\Request;

class EloquentController extends Controller
{
    //

    public function eloquent() {

        $d['model']= Orders::get();

        // dd($d['model']);
        
        return view('eloquent.index',$d);
    }
}
