<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueryController extends Controller
{
    //

    public function queryBuilder() {

        DB::enableQueryLog();

        // $d['model']=DB::table('customers')
        //             ->select('CompanyName','ContactName','ContactTitle')
        //             ->get();

        $d= '1 or 1';
        // $result= DB::select("SELECT * FROM orders where OrderID=$d");

        $result= DB::select("SELECT * FROM orders where OrderID=:id",['id'=>$d]);

        $querylast= DB::getQueryLog();

        dd($result);

        // dd($d['model']);

        return view('query.index',$d);
    }
}
