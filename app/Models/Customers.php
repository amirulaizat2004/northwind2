<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $table = 'Customers';

    protected $primaryKey = 'CustomerID';

    protected $fillable = ['CompanyName', 'ContactName', 'ContactTitle','Address', 'City', 'Region', 'PostalCode','Country', 'Phone', 'Fax'];


    public function order()
    {
        return $this->hasMany('App\Models\Orders');
    }

}
