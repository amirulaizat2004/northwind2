<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    use HasFactory;

    protected $table = 'Order Details';

    // protected $primaryKey = 'OrderID';

    protected $fillable = ['ProductID', 'UnitPrice', 'Quantity','Discount'];
}
