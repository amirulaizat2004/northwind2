<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $table = 'Orders';

    protected $primaryKey = 'OrderID';

    protected $fillable = ['CustomerID', 'EmployeeID', 'OrderDate','RequiredDate', 'ShippedDate', 'ShipVia','Freight', 'ShipName', 'ShipAddress','ShipCity', 'ShipRegion', 'ShipPostalCode','ShipCountry'];


    public function customers()
    {
        return $this->belongsTo('App\Models\Customers', 'CustomerID' , 'CustomerID');
    }

   
}
