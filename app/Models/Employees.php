<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = 'Employees';

    protected $primaryKey = 'EmployeeID';

    protected $fillable = ['LastName', 'FirstName', 'Title','TitleOfCourtesy', 'BirthDate', 'HireDate', 'Address','City', 'Region', 'PostalCode', 'Country', 'HomePhone','Extension', 'Photo', 'Notes', 'ReportsTo','PhotoPath', 'Salary'];

    public function order()
    {
        return $this->hasMany('App\Models\Orders');
    }
}
