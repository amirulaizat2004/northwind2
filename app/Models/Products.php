<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $table = 'Products';

    protected $primaryKey = 'ProductID';

    protected $fillable = ['ProductName', 'SupplierID', 'CategoryID','QuantityPerUnit', 'UnitPrice', 'UnitsInStock','UnitsOnOrder', 'ReorderLevel', 'Discontinued'];

}
