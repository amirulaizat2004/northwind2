@extends('layouts.app')

@section('content')


<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Company Name</th>
        <th scope="col">Contact Name</th>
        <th scope="col">Contact Title</th>
      </tr>
    </thead>

    <tbody>
        @php
            $bill=1;
        @endphp
        @foreach ($model as $item)

        <tr>
            <th scope="row">{{ $bill++ }}</th>
            <td>{{ $item->CompanyName??'' }}</td>
            <td>{{ $item->ContactName??'' }}</td>
            <td>{{ $item->ContactTitle??'' }}</td>
        </tr>

        @endforeach
      
    </tbody>
</table>


    
@endsection